/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplopropiedades;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author catalinaaguayo
 */
public class PruebaArchivoPropiedades {
    public static void main(String[] args) {
       
        Properties propiedades = new Properties();
        OutputStream salida = null;
        
        try {
            salida = new FileOutputStream("configuracion.propiedades");
            
            // asignamos los valores a las propiedades
            propiedades.setProperty("variabledemo","codehero");
            propiedades.setProperty("variabletest","test");
            
            // guardamos el archivo de propiedades en la carpeta de aplicación
            propiedades.store(salida, null);
                salida.close();
        }
        catch (IOException io) {
            io.printStackTrace();    
        }
    }
    
}
