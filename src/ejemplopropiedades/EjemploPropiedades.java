/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplopropiedades;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *
 * @author catalinaaguayo
 */
public class EjemploPropiedades {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Properties propiedades = new Properties();
        InputStream entrada = null;
        try {
            entrada = new FileInputStream("configuracion.propiedades");
            propiedades.load(entrada);
            
            System.out.println(propiedades.getProperty("variabledemo"));;
                entrada.close();      
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
}
